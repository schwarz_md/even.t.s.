INSERT INTO eventtickets.events (description, imageurl, name) VALUES (
  'Get ready to rock your night away with this megaconcert extravaganza from 10 of the biggest rock stars of the 80''''s',
  'https://dl.dropbox.com/u/65660684/640px-Weir%2C_Bob_(2007)_2.jpg', 'Rock concert of the decade');


INSERT INTO eventtickets.address (city, street, zipcode) VALUES ('Magdeburg', 'Egal', 39104);
INSERT INTO eventtickets.address (city, street, zipcode) VALUES ('Berlin', 'Egal 55', 11111);
INSERT INTO eventtickets.address (city, street, zipcode) VALUES ('München', 'Strasse', 89012);
INSERT INTO eventtickets.address (city, street, zipcode) VALUES ('Egal', 'Str', 12345);
INSERT INTO eventtickets.address (city, street, zipcode) VALUES ('aaa', 'www', 123);
INSERT INTO eventtickets.address (city, street, zipcode) VALUES ('qaywsx', 'yxcv', 12345);
INSERT INTO eventtickets.address (city, street, zipcode) VALUES ('qwe', 'ert', 123);
INSERT INTO eventtickets.address (city, street, zipcode) VALUES ('qweq', 'ertq', 123);
INSERT INTO eventtickets.address (city, street, zipcode) VALUES ('ffff', 'wwww', 123);
INSERT INTO eventtickets.address (city, street, zipcode) VALUES ('www', 'www', 123);


INSERT INTO eventtickets.venues (description, imageurl, name, addressid) VALUES (
  'Designed by McKim, Mead and White, it was built in 1900 for the Boston Symphony Orchestra, which continues to make the hall its home. The hall was designated a U.S. National Historic Landmark in 1999.',
  'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c0/Boston_Symphony_Hall_from_the_south.jpg/800px-Boston_Symphony_Hall_from_the_south.jpg',
  'Boston Symphony Hall', 1);


INSERT INTO eventtickets.users (username, email, enabled, firstname, lastname, password, addressid) VALUES
  ('alex', 'alex@test.de', TRUE, 'Alex', 'Test', '$2a$10$04TVADrR6/SPLBjsK0N30.Jf5fNjBugSACeGv1S69dZALR7lSov0y', 2);
INSERT INTO eventtickets.users (username, email, enabled, firstname, lastname, password, addressid)
VALUES ('fred', 'test@test', TRUE, 'qwer', 'asdf', '$2a$10$8ufRZFbMkKf0ivdzx96owu7DmOe3DNGHd9TceHuQhGU.9dmraIBUi', 6);
INSERT INTO eventtickets.users (username, email, enabled, firstname, lastname, password, addressid) VALUES
  ('fred2', 'fred2@test.de', TRUE, 'Fred', 'Test2', '$2a$10$A4bzdfd5e9UyTvu.OLPbw.T94yBEzUYkqKbqJ9mjTMEffefnnEwni', 4);
INSERT INTO eventtickets.users (username, email, enabled, firstname, lastname, password, addressid)
VALUES ('fred3', 'fred@test', TRUE, 'fred', 'Test3', '$2a$10$kXKe1AK1Rk1Gq5mD3fWKLOOWRwUnBE3jppmwcTGvyZ9GLB3iKiZFi', 5);
INSERT INTO eventtickets.users (username, email, enabled, firstname, lastname, password, addressid) VALUES
  ('fred4', 'fred@test4', TRUE, 'Fred', 'Test4', '$2a$10$32EJAP4DCL8YUCnMs4PEjOkm1pMsRukwxftHm9cHiw.EKEO86GPpm', 7);
INSERT INTO eventtickets.users (username, email, enabled, firstname, lastname, password, addressid) VALUES
  ('fred5', 'fred@test5', TRUE, 'Fred', 'Test5', '$2a$10$PlRtSfqvn1KFv6v5kwWO8uF//aQhx4YG1YB5oKW0IHKxgStc93h9O', 8);
INSERT INTO eventtickets.users (username, email, enabled, firstname, lastname, password, addressid) VALUES
  ('fred7', 'test@test5.de', TRUE, 'fred5', 'test', '$2a$10$5nid6nF9TWYc0gYRgjS1C.pD7g2Wlmoj4ud6s9s.DL87oZGTHkMKG', 10);
INSERT INTO eventtickets.users (username, email, enabled, firstname, lastname, password, addressid) VALUES
  ('thomas', 'test@test.de', TRUE, 'Thomas', 'Test', '$2a$10$04TVADrR6/SPLBjsK0N30.Jf5fNjBugSACeGv1S69dZALR7lSov0y', 1);
INSERT INTO eventtickets.users (username, email, enabled, firstname, lastname, password, addressid)
VALUES ('www', 'www@www.de', TRUE, 'www', 'www', '$2a$10$aiv6H/8hw58tyG3CR53zQeSU436y8tl3vr6s/i4d7bv7JCReBmHf6', 9);


INSERT INTO eventtickets.user_roles (role, username) VALUES ('ROLE_ADMIN', 'fred2');
INSERT INTO eventtickets.user_roles (role, username) VALUES ('ROLE_ADMIN', 'thomas');
INSERT INTO eventtickets.user_roles (role, username) VALUES ('ROLE_USER', 'alex');
INSERT INTO eventtickets.user_roles (role, username) VALUES ('ROLE_USER', 'fred');
INSERT INTO eventtickets.user_roles (role, username) VALUES ('ROLE_USER', 'fred2');
INSERT INTO eventtickets.user_roles (role, username) VALUES ('ROLE_USER', 'fred3');
INSERT INTO eventtickets.user_roles (role, username) VALUES ('ROLE_USER', 'fred4');
INSERT INTO eventtickets.user_roles (role, username) VALUES ('ROLE_USER', 'fred5');
INSERT INTO eventtickets.user_roles (role, username) VALUES ('ROLE_USER', 'fred7');
INSERT INTO eventtickets.user_roles (role, username) VALUES ('ROLE_USER', 'thomas');
INSERT INTO eventtickets.user_roles (role, username) VALUES ('ROLE_USER', 'www');


INSERT INTO eventtickets.shows (date, description, imageurl, eventid, venueid) VALUES ('2016-12-01', NULL, NULL, 1, 1);
INSERT INTO eventtickets.shows (date, description, imageurl, eventid, venueid) VALUES ('2016-12-16', NULL, NULL, 1, 1);


INSERT INTO eventtickets.sections (capacity, description, venueid) VALUES (100, 'Center', 1);


INSERT INTO eventtickets.pricecat (category) VALUES ('Adult');
INSERT INTO eventtickets.pricecat (category) VALUES ('Child');


INSERT INTO eventtickets.price (price, pricecatid_id, sectionid_id, showid_id) VALUES (10.00, 1, 1, 2);
INSERT INTO eventtickets.price (price, pricecatid_id, sectionid_id, showid_id) VALUES (5.00, 2, 1, 2);
