#Ticket Pokémon

##Zielsetzung

`Ticket Pokémon` ist eine Onlineverkaufsplattform für Veranstaltungstickets. 

Veranstalter können u.a.:

  * Veranstaltungsorte anlegen
    * Name
    * Bild
    * Beschreibung
    * Bereiche
      * Anzahl Reihen
      * Anzahl Plätze pro Reihe

  * Events anlegen
    * Name
    * Bild
    * Beschreibung
    * Art der Veranstaltung
    
Benutzer der Verkaufsplattform können u.a.:
  
  * Tickets für ein Event in verschiedenen Kategorien kaufen
  * Käufe stornieren

"Externe" Systeme können:

  * eine Liste der aktuellen Events über einen RESTful Service abrufen (JSON)


Die Applikation `Ticket Pokémon` ist als Spring MVC Anwendung als *Einzelprojekt* zu realisieren. 
Dabei sollen u.a. folgende Technologien eingesetzt werden:

  * JPA, Hibernate
  * JSP (Alternativen sind erlaubt)
  * jQuery, Bootstrap
  * HTML5
  * JAX-RS

##Spezifikation

Das System ist unter Verwendung geeigneter UML-Diagrammarten zu modellieren, u.a.: 

  * Komponentendiagramm
  * Use-Case-Diagramm
  * Aktivitätsdiagramm
  * Sequenzdiagramm

##Hinweise

Die Aufgabenstellung entspricht in abgespeckter Form dem `Ticket Monster Demo` Projekt, 
einem JavaEE-Tutorialprojekt für den JBoss-Applikationsserver.

JBoss:  http://www.jboss.org/get-started/
Github: https://github.com/jboss-developer/ticket-monster
