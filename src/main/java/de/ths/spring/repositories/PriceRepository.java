package de.ths.spring.repositories;

import de.ths.spring.model.Price;
import de.ths.spring.model.Show;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Thomas Schwarz on 30.09.16.
 */
public interface PriceRepository extends JpaRepository<Price, Long> {
    List<Price> findByShowid(Show show);
}
