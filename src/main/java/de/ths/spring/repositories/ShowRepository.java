package de.ths.spring.repositories;

import de.ths.spring.model.Event;
import de.ths.spring.model.Show;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Thomas Schwarz on 30.09.16.
 */
public interface ShowRepository extends JpaRepository<Show, Long> {
    List<Show> findByEvent(Event event);
}
