package de.ths.spring.repositories;

/**
 * Created by Thomas Schwarz on 30.09.16.
 */

import de.ths.spring.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {

}