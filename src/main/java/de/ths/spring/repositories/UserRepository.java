package de.ths.spring.repositories;

/**
 * Created by Thomas Schwarz on 30.09.16.
 */

import de.ths.spring.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {

    User findByUsername(String username);

    /*@Override
    User save(User user);*/
}