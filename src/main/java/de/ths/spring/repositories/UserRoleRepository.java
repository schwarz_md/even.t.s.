package de.ths.spring.repositories;

/**
 * Created by Thomas Schwarz on 30.09.16.
 */
import de.ths.spring.model.UserRole;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRoleRepository extends CrudRepository<UserRole, Integer> {

    @Query("SELECT ur.role FROM UserRole ur GROUP BY role")
    List<String> findRoles();
}