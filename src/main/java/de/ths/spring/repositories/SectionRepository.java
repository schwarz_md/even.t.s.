package de.ths.spring.repositories;

import de.ths.spring.model.Section;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Thomas Schwarz on 30.09.16.
 */
public interface SectionRepository extends JpaRepository<Section, Long> {
}
