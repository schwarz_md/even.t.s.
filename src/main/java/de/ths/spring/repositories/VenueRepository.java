package de.ths.spring.repositories;

/**
 * Created by Thomas Schwarz on 30.09.16.
 */

import de.ths.spring.model.Venue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VenueRepository extends JpaRepository<Venue, Long> {

}