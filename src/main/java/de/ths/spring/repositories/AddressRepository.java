package de.ths.spring.repositories;

/**
 * Created by Thomas Schwarz on 05.10.16.
 */

import de.ths.spring.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {

}