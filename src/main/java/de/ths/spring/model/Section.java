package de.ths.spring.model;

/**
 * Created by Thomas Schwarz on 30.09.16.
 */
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "sections")
public class Section {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /*@ManyToOne
    @JoinColumn(name = "venueid", nullable = false)
    private Venue venue;
*/
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "sectionid")
    private List<Price> prices;

    private String description;
    private Long capacity;
}
