package de.ths.spring.model;

/**
 * Created by Thomas Schwarz on 30.09.16.
 */
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "shows")
public class Show {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "eventid", nullable = false)
    private Event event;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "venueid", nullable = false)
    private Venue venue;

    private java.sql.Date date;
    private String description;
    private String imageurl;
}
