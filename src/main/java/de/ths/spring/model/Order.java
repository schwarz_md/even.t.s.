package de.ths.spring.model;

/**
 * Created by Thomas Schwarz on 30.09.16.
 */
import lombok.Data;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Data
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "userid", nullable = false)
    private User user;

    private Date date;

    private Boolean canceled;

    @OneToMany(fetch = FetchType.LAZY)
    @Column(name = "ticketid")
    private List<Ticket> tickets;

}
