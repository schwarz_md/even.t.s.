package de.ths.spring.model;

/**
 * Created by Thomas Schwarz on 30.09.16.
 */
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@ToString(exclude = "shows")
@Table(name = "venues")
public class Venue {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "venue")
    private List<Show> shows;

    @OneToMany(fetch = FetchType.LAZY)
    private List<Section> sections;

    @ManyToOne
    @JoinColumn(name = "addressid", nullable = false)
    private Address address;

    private String description;
    private String imageurl;
}
