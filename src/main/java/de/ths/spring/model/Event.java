package de.ths.spring.model;

/**
 * Created by Thomas Schwarz on 30.09.16.
 */
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@ToString(exclude = "shows")
@Table(name = "events")
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "event")
    private List<Show> shows;

    private String name;
    private String description;
    private String imageurl;

}
