package de.ths.spring.model;

/**
 * Created by Thomas Schwarz on 30.09.16.
 */
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "tickets")
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long seatnumber;

    @ManyToOne
    @JoinColumn(name = "priceid", nullable = false)
    private Price price;
}
