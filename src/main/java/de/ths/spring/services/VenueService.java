package de.ths.spring.services;

/**
 * Created by Thomas Schwarz on 30.09.16.
 */
import de.ths.spring.model.Venue;
import de.ths.spring.repositories.VenueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class VenueService {

    @Autowired
    private VenueRepository venueRepository;


    public Page<Venue> findAll(Pageable pageable) {
        return venueRepository.findAll(pageable);
    }

    public List<Venue> findAll() {
        return venueRepository.findAll();
    }

}
