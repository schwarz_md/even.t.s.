package de.ths.spring.services;

/**
 * Created by Thomas Schwarz on 30.09.16.
 */

import de.ths.spring.model.User;
import de.ths.spring.model.UserRole;
import de.ths.spring.repositories.AddressRepository;
import de.ths.spring.repositories.UserRepository;
import de.ths.spring.repositories.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserRoleRepository userRoleRepository;
    @Autowired
    private AddressRepository addressRepository;


    public Boolean newUser(User user) {
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        user.setEnabled(true);
        if (userRepository.findOne(user.getUsername()) == null) {
            addressRepository.save(user.getAddress());
            userRoleRepository.save(new UserRole(userRepository.save(user), "ROLE_USER"));
            return true;
        }
        return false;
    }

    public User updateUser(User user) {
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        user.setEnabled(true);
        addressRepository.save(user.getAddress());
        userRoleRepository.save(new UserRole(userRepository.save(user), "ROLE_USER"));

        return user;
    }

    public Page<User> findAll(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }
}
