package de.ths.spring.services;

/**
 * Created by Thomas Schwarz on 30.09.16.
 */
import de.ths.spring.model.Event;
import de.ths.spring.model.Show;
import de.ths.spring.repositories.EventRepository;
import de.ths.spring.repositories.ShowRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class EventService {

    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private ShowRepository showRepository;


    public Page<Event> findAll(Pageable pageable) {
        return eventRepository.findAll(pageable);
    }

    public List<Event> findAll() {
        return eventRepository.findAll();
    }

    public Event findOne(Long id) {
        return eventRepository.findOne(id);
    }

    public List<Show> findByEventid(Event event) {
        return showRepository.findByEvent(event);
    }
}
