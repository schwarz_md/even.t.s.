package de.ths.spring.controllers;

/**
 * Created by Thomas Schwarz on 13.10.16.
 */

import de.ths.spring.model.Show;
import de.ths.spring.repositories.PriceRepository;
import de.ths.spring.services.ShowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@Secured("ROLE_ADMIN")
public class OrderController {

    @Autowired
    private ShowService showService;

    @Autowired
    private PriceRepository priceRepository;

    @RequestMapping(value = "/order/{showid}", method = RequestMethod.GET)
    public String list(@PathVariable Long showid, Model model) {
        Show show = showService.findOne(showid);
        model.addAttribute("show", show);
        //model.addAttribute("price", priceRepository.findByShowid(show));
        return "order";
    }

}