package de.ths.spring.controllers;

/**
 * Created by Thomas Schwarz on 30.09.16.
 */

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/admin")
@Secured("ROLE_ADMIN")
public class AdminController {


}
