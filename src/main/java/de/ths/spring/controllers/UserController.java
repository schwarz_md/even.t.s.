package de.ths.spring.controllers;

/**
 * Created by Thomas Schwarz on 30.09.16.
 */

import de.ths.spring.model.User;
import de.ths.spring.model.UserRole;
import de.ths.spring.repositories.UserRepository;
import de.ths.spring.repositories.UserRoleRepository;
import de.ths.spring.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
@Secured("ROLE_ADMIN")
public class UserController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserRoleRepository userRoleRepository;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("users", userRepository.findAll());
        System.out.println("Returning users:");
        for (User u : userRepository.findAll()) {
            System.out.println(u);
        }
        return "users";
    }

    @RequestMapping(value = "/usersroles", method = RequestMethod.GET)
    public String listUR(Model model) {
        model.addAttribute("userroles", userRoleRepository.findAll());
        System.out.println("Returning userRoles:");
        for (UserRole u : userRoleRepository.findAll()) {
            System.out.println(u.toString());
        }
        return "usersroles";
    }

    @RequestMapping(value = "/userupdate", method = RequestMethod.GET)
    public String userupdate(Model model) {
        User user = userRepository.findOne(getUserName());
        model.addAttribute("user", user);
        System.out.println("\n+++ " + user + " +++\n");
        return "userform";
    }

    @RequestMapping(value = "/userregister", method = RequestMethod.GET)
    public String register(Model model) {
        model.addAttribute("user", new User());
        return "userform";
    }

    @RequestMapping(value = "/userregister", method = RequestMethod.POST)
    public String registerSucc(@Valid User user, BindingResult result, Model model) {
        user.setEnabled(true);
        System.out.println("\n*** " + user + " ***\n");
        if (userService.newUser(user)) return "userRSuccess";
        else return "userform";
    }

    private String getUserName() {
        String result = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            result = ((UserDetails) principal).getUsername();
        } else {
            result = principal.toString();
        }
        return result;// + " ###" + ((UserDetails) principal).getAuthorities().toString();
    }

}
