package de.ths.spring.controllers;

/**
 * Created by Thomas Schwarz on 30.09.16.
 */
import de.ths.spring.model.Event;
import de.ths.spring.services.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@Secured("ROLE_ADMIN")
public class EventController {

    @Autowired
    private EventService eventService;

    @RequestMapping(value = "/events", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("events", eventService.findAll(new PageRequest(0, 5)));
        System.out.println("Returning events:");
        for (Event u : eventService.findAll()) {
            System.out.println(u);
        }
        return "events";
    }

    @RequestMapping(value = "/eventdetails/{id}", method = RequestMethod.GET)
    public String detaillist(@PathVariable Long id, Model model) {
        Event event = eventService.findOne(id);
        model.addAttribute("event", event);
        model.addAttribute("shows", eventService.findByEventid(event));
        System.out.println("Returning eventdetails");

        return "eventdetails";
    }

}
