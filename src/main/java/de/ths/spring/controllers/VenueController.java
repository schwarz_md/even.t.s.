package de.ths.spring.controllers;

/**
 * Created by Thomas Schwarz on 30.09.16.
 */
import de.ths.spring.model.Venue;
import de.ths.spring.services.VenueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class VenueController {

    @Autowired
    private VenueService venueService;

    @RequestMapping(value = "/venues", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("venues", venueService.findAll());
        System.out.println("Returning venues:");
        for (Venue u : venueService.findAll()) {
            System.out.println(u);
        }
        return "venues";
    }

}
